package cat.itb.yugiohcardsviewer.DataClasses

data class CardSet(
    val set_code: String,
    val set_name: String,
    val set_price: String,
    val set_rarity: String,
    val set_rarity_code: String
)