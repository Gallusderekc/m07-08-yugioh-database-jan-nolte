package cat.itb.yugiohcardsviewer

import cat.itb.yugiohcardsviewer.DataClasses.Data
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Url


interface ApiInterface {
    // https://db.ygoprodeck.com/api-guide/

    @GET()
    fun getData(@Url url: String): Response<Data>

    @GET()
    suspend fun getCards(@Url url: String): Response<Data>

    companion object {
        val BASE_URL = "https://db.ygoprodeck.com/api/v7/"

        fun create(): ApiInterface {
            val client = OkHttpClient.Builder().build()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()

            return retrofit.create(ApiInterface::class.java)
        }
    }

}