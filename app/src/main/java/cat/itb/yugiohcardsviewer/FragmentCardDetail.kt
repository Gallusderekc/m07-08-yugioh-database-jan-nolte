package cat.itb.yugiohcardsviewer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import cat.itb.yugiohcardsviewer.DataBase.CardApplication
import cat.itb.yugiohcardsviewer.DataBase.CardEntity
import cat.itb.yugiohcardsviewer.DataClasses.CardYugioh
import cat.itb.yugiohcardsviewer.databinding.FragmentCardDetailBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.coroutines.CoroutineContext

class FragmentCardDetail:Fragment() {
    private lateinit var binding: FragmentCardDetailBinding
    private val viewModel: AppViewModel by activityViewModels()
    private var cardPos:Int? = null
    private var card:CardYugioh? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentCardDetailBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if(arguments?.isEmpty != true){
            cardPos = arguments!!.get("ID_carta") as Int
            card = viewModel.cards.value!!.cardList[cardPos!!]
            println(card)
        }
        val url:String = card!!.card_images[0].image_url
        isThisCardInCollection(card!!.id)
        Glide.with(context!!)
            .load(url)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .fitCenter()
            .into(binding.cardImage)
        binding.cardName.text = card!!.name
        binding.cardAtributte.text = card!!.attribute
        binding.cardAttack.text = card!!.atk.toString()
        binding.cardDefense.text = card!!.def.toString()
        binding.cardDescription.text = card!!.desc
        binding.cardLevel.text = card!!.level.toString()
        binding.cardType.text = card!!.type
        binding.cardInCollection.setOnClickListener{
            val checkbox = it as CheckBox
            if(checkbox.isChecked){
                println("add")
                CoroutineScope(Dispatchers.IO).launch {
                    val newCard = CardEntity(id = card!!.id, name = card!!.name, cardImage = card!!.card_images[0].image_url)
                    CardApplication.database.cardsDAO().addToCollection(newCard)
                }
            }else{
                println("del")
                CoroutineScope(Dispatchers.IO).launch {
                    val deletedCard = CardEntity(id = card!!.id, name = card!!.name, cardImage = card!!.card_images[0].image_url)
                    CardApplication.database.cardsDAO().deleteFromCollection(deletedCard)
                }
            }
        }
    }
    private fun isThisCardInCollection(id:Int){
        CoroutineScope(Dispatchers.IO).launch {
            val databaseResponse = CardApplication.database.cardsDAO().searchInCollection(id)
            withContext(Dispatchers.Main){
                binding.cardInCollection.isChecked = databaseResponse>0
                println(databaseResponse)
            }
        }
    }

}
