package cat.itb.yugiohcardsviewer.DataBase

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = arrayOf(CardEntity::class), version = 1)
abstract class CardDatabase:RoomDatabase(){
    abstract fun cardsDAO():CardsDAO
}